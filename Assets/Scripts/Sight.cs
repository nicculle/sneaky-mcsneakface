﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public class Sight : MonoBehaviour
{
    [HideInInspector]
    public GameObject Target; //The Target that signifies the player. This is set by the enemy controller
    public float SightRange = 4f; //How far the enemy can see
    public float FOV = 45f; //How wide the FOV of the enemy is
    [Space]
    [Space]
    [Header("Debug")]
    [SerializeField]
    private bool DebugLines = false;
    [SerializeField]
    private Color RadiusColor = default;
    [SerializeField]
    private Color FOVColor = default;
    
    //Returns true if the enemy is seeing the player, and false otherwise
    public bool SeeingPlayer
    {
        get
        {
            //If there is a target
            if (Target != null)
            {
                //Check if the player is within seeing distance and is within the FOV
                if (Vector3.Distance(Target.transform.position, transform.position) <= SightRange && Vector3.Angle(transform.right, Target.transform.position - transform.position) <= FOV)
                {
                    //Fire a raycast towards the player
                    var hit = Physics2D.Raycast(transform.position, (Target.transform.position - transform.position).normalized, SightRange, LayerMask.GetMask("Default", "Player"));
                    //If the collision is the player, then the enemy can see the player
                    if (hit.collider != null && hit.collider.gameObject == Target)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private void Update()
    {
        if (DebugLines)
        {
            //Draw a circle around the enemy signifying the sight range
            DebugExt.DrawCircle2D(transform.position, SightRange,color: RadiusColor);
            var direction = transform.rotation.eulerAngles.z;
            var leftSide = direction - (FOV / 2f);
            var rightSide = direction + (FOV / 2f);
            var leftVector = transform.position + new Vector3(Mathf.Cos(leftSide * Mathf.Deg2Rad) * SightRange, Mathf.Sin(leftSide * Mathf.Deg2Rad) * SightRange);
            var rightVector = transform.position + new Vector3(Mathf.Cos(rightSide * Mathf.Deg2Rad) * SightRange, Mathf.Sin(rightSide * Mathf.Deg2Rad) * SightRange);
            //Draw the Lines Signifying the FOV
            Debug.DrawLine(transform.position, leftVector,FOVColor);
            Debug.DrawLine(transform.position, rightVector,FOVColor);
        }
    }
}
