﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public class Hearing : MonoBehaviour
{
    [HideInInspector]
    public GameObject Target; //The Target that signifies the player. This is set by the enemy controller
    public float HearingRange = 5f; //The hearing range of the enemy
    public bool HearingPlayer => Vector3.Distance(Target.transform.position, transform.position) <= HearingRange; //Returns true if the player is within hearing radius
    [Space]
    [Space]
    [Header("Debug")]
    [SerializeField]
    private bool DebugLines = false;
    [SerializeField]
    private Color RadiusColor = default;

    private void Update()
    {
        if (DebugLines)
        {
            //Draw a circle signifying the hearing range
            DebugExt.DrawCircle2D(transform.position, HearingRange, color: RadiusColor);
        }
    }
}
