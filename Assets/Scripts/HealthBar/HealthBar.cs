﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HealthBar : MonoBehaviour
{
    [SerializeField]
    protected float health = 100f; //The starting health of the player
    public float Health { get => health; set => UpdateHealth(value); } //The public interface for accessing the health
    public float MaxHealth { get; private set; } //Public interface for accessing the maximum health value

    public virtual void Start()
    {
        MaxHealth = health; //Set the max health to the currently set health
    }

    protected virtual void UpdateHealth(float newHealth)
    {
        //Clamp the newHealth between 0 and MaxHealth and update the health
        health = Mathf.Clamp(newHealth, 0, MaxHealth);
    }
}
