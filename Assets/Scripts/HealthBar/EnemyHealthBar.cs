﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class EnemyHealthBar : HealthBar
{
    private RectTransform rectTransform; //The rect transform of the health area
    private RectTransform parentCanvas; //The rect transform of the canvas. Used to get the canvas width

    public override void Start()
    {
        base.Start(); //Call the base function
        rectTransform = GetComponent<RectTransform>(); //Get the rect transform of this object
        parentCanvas = GetComponentInParent<Canvas>().GetComponent<RectTransform>(); //Get the rect transform of the object's canvas
    }

    protected override void UpdateHealth(float newHealth)
    {
        //Call the base function
        base.UpdateHealth(newHealth);
        //Change the width of the health image indicator to reflect the current health
        rectTransform.offsetMax = new Vector2(-parentCanvas.rect.width * (1 - (health / MaxHealth)), rectTransform.offsetMax.y);
    }
}
