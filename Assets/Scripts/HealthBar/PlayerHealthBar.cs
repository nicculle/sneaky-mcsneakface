﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerHealthBar : HealthBar
{
    public static PlayerHealthBar PlayerHealth { get; private set; } //The Singleton of the PlayerHealthBar to make it easily accessable
    private RectTransform imageTransform; //The rect transform of the health image
    public RectTransform canvasTransform; //The rect transfrom of the canvas the image is in. Used to get the canvas width

    private float baseWidth; //The width of the health area image at full health
    private float? emptyWidth; //The width of the health area image at zero health

    public void Awake()
    {
        //Call the base start class
        base.Start();
        //Get the rect transform of this object
        imageTransform = GetComponent<RectTransform>();
        //Get the base width when the health is at max
        baseWidth = -imageTransform.offsetMax.x;
        //Set the playerHealth singleton
        if (PlayerHealth == null)
        {
            PlayerHealth = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //Called when the health is updated
    protected override void UpdateHealth(float newHealth)
    {
        //Get the empty width that is used when the health bar is empty
        if (emptyWidth == null)
        {
            emptyWidth = canvasTransform.rect.width;
        }
        //Call the base function
        base.UpdateHealth(newHealth);
        //Update the width of the of the health bar
        imageTransform.offsetMax = new Vector2(-Mathf.Lerp(baseWidth,emptyWidth.Value,1 - (health / MaxHealth)), imageTransform.offsetMax.y);
    }
}
