﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedRotation : MonoBehaviour
{
    Quaternion rotation = Quaternion.identity; //The rotation that will be set
    private void LateUpdate()
    {
        transform.rotation = rotation; //Set the global rotation of the gameObject to be the set rotation value
    }
}
