﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

//The states the game will play in
public enum GameState
{
    MainMenu, //The main menu
    Playing, //The main gameplay
    Win, //The win screen
    Lose //The loose screen
}

public class GameManager : MonoBehaviour
{
    public static GameManager Singleton { get; private set; } //The singleton of the GameManager
    public GameObject PlayerBulletPrefab; //The prefab for the bullet the player shoots
    public GameObject EnemyBulletPrefab; //The prefab for the bullet the enemy shoots
    public static bool GameFinished { get; private set; } //Is set to true when the game is over

    public void ChangeGameState(GameState newState)
    {
        switch (newState)
        {
            case GameState.MainMenu:
                //Enable the main menu screen
                UIController.SetUIState("Main Menu");
                //Set the Menu scene to be the active one
                SceneManager.SetActiveScene(SceneManager.GetSceneByName("Menus"));
                //Unload the main game scene
                SceneManager.UnloadSceneAsync("Main Scene");
                break;
            case GameState.Playing:
                //Load the main game scene
                StartCoroutine(LoadLevel());
                break;
            case GameState.Win:
                UIController.SetUIState("Win");
                break;
            case GameState.Lose:
                UIController.SetUIState("Lose");
                break;
        }
    }

    //Loads the level in
    private IEnumerator LoadLevel()
    {
        //Load the main game scene and wait until it completes
        yield return SceneManager.LoadSceneAsync("Main Scene", LoadSceneMode.Additive);
        //Set the main game scene to be the active one
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Main Scene"));
        //Enable the game UI
        UIController.SetUIState("Game");
    }


    private void Start()
    {
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    //Called when all the enemies are destroyed
    public void Win()
    {
        //Change to the win state
        ChangeGameState(GameState.Win);
        //Finish the game
        GameFinished = true;
    }
    //Called when the player dies
    public void GameOver()
    {
        //Change the state to loose
        ChangeGameState(GameState.Lose);
        //Finish the game
        GameFinished = true;
    }
    //Called when the "Play" button is pressed
    public void Play()
    {
        //Change the state to playing
        ChangeGameState(GameState.Playing);
    }

    //Called when any "Main Menu" button is pressed
    public void MainMenu()
    {
        ChangeGameState(GameState.MainMenu);
        //Destroy clear the enemy and player list
        EnemyPawn.Enemies.Clear();
        PlayerPawn.Players.Clear();
        //Reset the "GameFinished" variable
        GameFinished = false;
    }

    //Called when the Quit button is pressed
    public void QuitGame()
    {
        //Quit the game
        Application.Quit();
    }
}
