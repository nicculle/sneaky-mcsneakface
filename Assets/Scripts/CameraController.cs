﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Target; //The target the camera will keep track of
    public float Speed = 7f; //How fast the camera moves to the player
    // Start is called before the first frame update
    void Start()
    {
        //If there is a target
        if (Target != null)
        {
            //Set the position of the camera to the target's
            transform.position = new Vector3(Target.transform.position.x, Target.transform.position.y, transform.position.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //If there is a target
        if (Target != null)
        {
            //Move towards the target
            transform.position = Vector3.Lerp(transform.position, new Vector3(Target.transform.position.x, Target.transform.position.y, transform.position.z), Speed * Time.deltaTime);
        }
    }
}
