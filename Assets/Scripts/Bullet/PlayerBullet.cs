﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PlayerBullet : Bullet
{
    protected override string TargetLayer => "Enemy"; //The enemy will get damaged by this bullet
}
