﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    public float Speed = 7f; //The speed at which the bullet travels
    public float Damage = 10f; //The amount of damage the bullet does to an opposing pawn
    public float BulletLifetime = 10f; //How long the bullet lives before it is automatically destroyed
    protected abstract string TargetLayer { get; } //The target layer that the bullet will deal damage to
    private Vector3 Direction; //The direction the bullet travels in
    private void Start()
    {
        //Set the direction of the bullet based on the current rotation
        Direction = new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad),Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad));
        //Destroy the bullet after the set amount of lifetime
        Destroy(gameObject, BulletLifetime);
    }

    private void Update()
    {
        //Move the bullet in the set direction
        transform.position += Direction * Time.deltaTime * Speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If the bullet has hit a wall
        if (collision.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            //Destroy the bullet
            Destroy(gameObject);
        }
        //If the bullet has hit the target
        if (collision.gameObject.layer == LayerMask.NameToLayer(TargetLayer))
        {
            //Decrease the target's health
            collision.GetComponent<Pawn>().Health -= Damage;
            //Destroy the bullet
            Destroy(gameObject);
        }
    }
}
