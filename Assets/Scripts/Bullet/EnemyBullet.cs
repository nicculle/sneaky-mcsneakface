﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class EnemyBullet : Bullet
{
    protected override string TargetLayer => "Player"; //The player will get damaged by this bullet
}
