﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class EnemyPawn : Pawn
{
    Rigidbody2D rigid; //The rigidbody2D component of the enemy
    public static List<EnemyPawn> Enemies = new List<EnemyPawn>(); //The total amount of enemies currently in the game

    public override void Start()
    {
        //Call the base function
        base.Start();
        //Get the rididbody of this object
        rigid = GetComponent<Rigidbody2D>();
        //Add this enemy to the enemies list
        Enemies.Add(this);
    }

    //Gets the health bar of the enemy
    protected override HealthBar GetHealthBar()
    {
        return GetComponentInChildren<HealthBar>();
    }

    //Moves the pawn forward
    public override void MoveForward()
    {
        rigid.velocity = transform.right * speed;
    }
    //Moves the pawn backward
    public override void MoveBackwards()
    {
        rigid.velocity = transform.right * -speed;
    }
    //Rotates the pawn to the left
    public override void RotateLeft()
    {
        rigid.angularVelocity = rotationSpeed;
    }
    //Rotates the pawn to the right
    public override void RotateRight()
    {
        rigid.angularVelocity = -rotationSpeed;
    }
    //Causes the pawn to stop moving
    public override void StopMoving()
    {
        rigid.velocity = Vector2.zero;
    }
    //Causes the pawn to stop rotating
    public override void StopRotating()
    {
        rigid.angularVelocity = 0;
    }
    //Causes the pawn to fire a bullet
    public override void FireBullet()
    {
        GameObject.Instantiate(GameManager.Singleton.EnemyBulletPrefab, transform.TransformPoint(BulletSpawnOffset), transform.rotation);
    }
    //Called when the pawn dies
    protected override void OnDie()
    {
        //Destroy this enemy
        Destroy(gameObject);
        //Remove this enemy from the enemy list
        Enemies.Remove(this);
        //If the total amount of enemies is zero
        //And there is a player still in the game
        if (Enemies.Count == 0 && PlayerPawn.Players.Count > 0)
        {
            //Trigger the game to win
            GameManager.Singleton.Win();
        }
    }
}
