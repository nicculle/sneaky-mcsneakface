﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerPawn : Pawn
{
    Rigidbody2D rigid;
    public static List<PlayerPawn> Players = new List<PlayerPawn>(); //The total amount of players currently in the game

    public override void Start()
    {
        base.Start();
        //Set the health to the maximum health
        Health = healthbar.MaxHealth;
        //Get the rigidbody component
        rigid = GetComponent<Rigidbody2D>();
        //Add this object to the list of players
        Players.Add(this);
    }

    protected override HealthBar GetHealthBar()
    {
        //Get the Player's Health bar in the UI
        return PlayerHealthBar.PlayerHealth;
    }

    //Moves the pawn forward
    public override void MoveForward()
    {
        rigid.velocity = transform.right * speed;
    }
    //Moves the pawn backward
    public override void MoveBackwards()
    {
        rigid.velocity = transform.right * -speed;
    }
    //Rotates the pawn to the left
    public override void RotateLeft()
    {
        rigid.angularVelocity = rotationSpeed;
    }
    //Rotates the pawn to the right
    public override void RotateRight()
    {
        rigid.angularVelocity = -rotationSpeed;
    }
    //Causes the pawn to stop moving
    public override void StopMoving()
    {
        rigid.velocity = Vector2.zero;
    }
    //Causes the pawn to stop rotating
    public override void StopRotating()
    {
        rigid.angularVelocity = 0;
    }
    //Called when the player presses space
    public override void FireBullet()
    {
        GameObject.Instantiate(GameManager.Singleton.PlayerBulletPrefab, transform.TransformPoint(BulletSpawnOffset), transform.rotation);
    }

    //Called when the player dies
    protected override void OnDie()
    {
        //Destroy the player
        Destroy(gameObject);
        //Remove the player from the list of players
        Players.Remove(this);
        //If there are no players 
        //and there are still enemies in the game
        if (Players.Count == 0 && EnemyPawn.Enemies.Count > 0)
        {
            //Cause the game to loose
            GameManager.Singleton.GameOver();
        }
    }
}
