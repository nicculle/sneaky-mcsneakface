﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    public float speed = 10.0f; //How fast the pawn will move per second
    public float rotationSpeed = 180.0f; //How fast the pawn will rotate per second
    protected virtual Vector2 BulletSpawnOffset => new Vector2(0.2f,-0.2f); //The offset the bullet spawns at

    protected HealthBar healthbar; //The healthbar of the pawn

    public float Health //The health of the enemy
    {
        get => healthbar.Health;
        set
        {
            if (value <= 0)
            {
                healthbar.Health = value;
                //Call the OnDie Function of the pawn
                OnDie();
            }
            else
            {
                healthbar.Health = value;
            }
        }
    }

    protected abstract HealthBar GetHealthBar(); //Used to get the health meter for the pawn
    protected abstract void OnDie(); //Called when the pawn's health reaches zero

    //The start function of the pawn
    public virtual void Start()
    {
        healthbar = GetHealthBar();
    }

    //The update function of the pawn
    public virtual void Update()
    {

    }

    //Moves the pawn forward
    public abstract void MoveForward();

    //Moves the pawn backward
    public abstract void MoveBackwards();

    //Stops the pawn from moving
    public abstract void StopMoving();

    //Rotates the pawn to the left
    public abstract void RotateLeft();

    //Rotates the pawn to the right
    public abstract void RotateRight();

    //Stops the pawn from rotating
    public abstract void StopRotating();

    public abstract void FireBullet();

}
