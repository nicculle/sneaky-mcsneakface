﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{ 
    public static UIController Singleton { get; private set; } //The singleton for the UI Controller

    [SerializeField]
    private string CurrentState = "Main Menu"; //The current UI State
    private static Dictionary<string, GameObject> CanvasList = new Dictionary<string, GameObject>(); //A list of all valid UI states

    // Start is called before the first frame update
    void Start()
    {
        //Keep the UI Controller always loaded
        DontDestroyOnLoad(gameObject);
        //Set the singleton
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        //Loop over the children and add them to the canvas list
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i).gameObject;
            CanvasList.Add(child.name, child);
        }
        //Set the current state (the main menu area) to be active
        CanvasList[CurrentState].SetActive(true);
    }

    //Sets the current UI State
    public static void SetUIState(string NewState)
    {
        //Get the old state
        var oldState = Singleton.CurrentState;
        //Set the new state
        Singleton.CurrentState = NewState;
        //Disable the old state
        CanvasList[oldState].SetActive(false);
        //Enable the new state if possible
        if (CanvasList.ContainsKey(NewState))
        {
            CanvasList[NewState].SetActive(true);
        }
        else
        {
            throw new System.Exception(NewState + " is an invalid UI State");
        }
    }
}
