﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Extensions
{
    public static class DebugExt
    {
        //A helper function used to draw a debug circle
        public static void DrawCircle2D(Vector2 origin, float Radius, int Subsections = 100,Color? color = null)
        {
            color = color ?? Color.white;
            var angleDifference = 360f / Subsections;
            for (int i = 0; i < Subsections; i++)
            {
                Vector3 start = new Vector3(origin.x,origin.y) + new Vector3(Mathf.Cos(angleDifference * i * Mathf.Deg2Rad) * Radius, Mathf.Sin(angleDifference * i * Mathf.Deg2Rad) * Radius);
                Vector3 end = new Vector3(origin.x, origin.y) + new Vector3(Mathf.Cos(angleDifference * (i + 1) * Mathf.Deg2Rad) * Radius, Mathf.Sin(angleDifference * (i + 1) * Mathf.Deg2Rad) * Radius);
                Debug.DrawLine(start, end,color.Value);
            }
        }
    }
}
