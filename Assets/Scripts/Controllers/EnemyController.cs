﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum EnemyStates
{
    Idle, //Used when the enemy cannot see nor hear the player
    LookAtPlayer, //Used when the enemy can hear the player, but cannot see them
    MoveToPlayer //Used when the enemy can see the player
}

public class EnemyController : Controller
{
    public GameObject PlayerTarget; //The player object that this enemy will be targeting
    public EnemyStates currentState; //The current state of the enemy
    public float FireRate = 0.5f; //Determines how many bullets fire per second

    private Hearing hearing; //The hearing component of this enemy
    private Sight sight; //The sight component of this enemy
    private float BulletClock = 0f; //The clock used to keep track of the fire rate

    public override void Start()
    {
        base.Start(); //Call the base function
        hearing = GetComponent<Hearing>(); //Get the hearing component
        hearing.Target = PlayerTarget; //Set the target of the hearing component to be the player object
        sight = GetComponent<Sight>(); //Get the sight component
        sight.Target = PlayerTarget; //Set the target of the sight component to be the player object
        StartCoroutine(SlowUpdate(10)); //Start up the slower update loop
    }

    IEnumerator SlowUpdate(int updatesPerSecond)
    {
        while (true)
        {
            //If the enemy can see the player
            if (PlayerTarget != null && sight.SeeingPlayer)
            {
                //Change the state to move towards the player
                currentState = EnemyStates.MoveToPlayer;
            }
            //If the enemy is hearing the player
            else if (PlayerTarget != null && hearing.HearingPlayer)
            {
                //Change the state to look at the player
                currentState = EnemyStates.LookAtPlayer;
            }
            //If the enemy cannot see or hear the player
            else
            {
                //Go to the idle state
                currentState = EnemyStates.Idle;
            }
            yield return new WaitForSeconds(1f / updatesPerSecond);
        }
    }

    public override void Update()
    {
        //If the game is still running
        if (!GameManager.GameFinished)
        {
            //If the enemy state is idle
            if (currentState == EnemyStates.Idle)
            {
                //Stop any movement
                pawn.StopMoving();
                //Stop any rotation
                pawn.StopRotating();
            }
            //If the enemy state is to look at the player
            else if (currentState == EnemyStates.LookAtPlayer)
            {
                //Stop any movement
                pawn.StopMoving();
                //Get the angle between the enemy and the player
                var angle = Vector3.SignedAngle(transform.right, PlayerTarget.transform.position - transform.position, Vector3.forward);
                //If the player is left of the player
                if (angle > 0)
                {
                    //Turn the enemy to the left
                    pawn.RotateLeft();
                }
                //If the player is right of the player
                else if (angle < 0)
                {
                    //Turn the enemy to the right
                    pawn.RotateRight();
                }
            }
            //If the enemy state is to move towards the player
            else if (currentState == EnemyStates.MoveToPlayer)
            {
                //Increase the bullet counter
                BulletClock += Time.deltaTime * FireRate;
                //Move the enemy forward
                pawn.MoveForward();
                var angle = Vector3.SignedAngle(transform.right, PlayerTarget.transform.position - transform.position, Vector3.forward);
                //If the player is left of the player
                if (angle > 0)
                {
                    //Turn the enemy to the left
                    pawn.RotateLeft();
                }
                //If the player is right of the player
                else if (angle < 0)
                {
                    //Turn the enemy to the right
                    pawn.RotateRight();
                }
                //If the bullet clock is greater than or equal to 1
                if (BulletClock >= 1)
                {
                    //Reset the counter
                    BulletClock = 0;
                    //Fire a bullet
                    pawn.FireBullet();
                }
            }
        }
        //If the game is over
        else
        {
            //Stop any movement
            pawn.StopMoving();
            //Stop any rotation
            pawn.StopRotating();
        }
    }
}
