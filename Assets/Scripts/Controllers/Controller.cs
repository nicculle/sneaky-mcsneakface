﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    [HideInInspector]
    public Pawn pawn; //The pawn object this controller controls

    public virtual void Start()
    {
        pawn = GetComponent<Pawn>(); //Get the pawn object
    }

    public abstract void Update();
}
