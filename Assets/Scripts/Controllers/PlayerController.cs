﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerController : Controller
{

    //Helper Controls
    private static bool UpHeld => Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
    private static bool DownHeld => Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
    private static bool LeftHeld => Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
    private static bool RightHeld => Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);

    public override void Update()
    {
        //If the game is still running
        if (!GameManager.GameFinished)
        {
            //If the up key is pressed
            if (UpHeld)
            {
                //Move the player forward
                pawn.MoveForward();
            }
            //If the down key is pressed
            else if (DownHeld)
            {
                //Move the player down
                pawn.MoveBackwards();
            }
            //If neither up or down keys are pressed
            else
            {
                //Stop any movement
                pawn.StopMoving();
            }
            //If the left key is pressed
            if (LeftHeld)
            {
                //Rotate the player to the left
                pawn.RotateLeft();
            }
            //If the right key is pressed
            else if (RightHeld)
            {
                //Rotate the player to the right
                pawn.RotateRight();
            }
            //If neither the left or right keys are pressed
            else
            {
                //Stop any rotation
                pawn.StopRotating();
            }
            //If the spacebar is pressed
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Fire a bullet
                pawn.FireBullet();
            }
        }
        //If the game is over
        else
        {
            //Stop any movement
            pawn.StopMoving();
            //Stop any rotation
            pawn.StopRotating();
        }
    }
}
